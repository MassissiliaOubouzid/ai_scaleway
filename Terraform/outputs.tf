output "kubeconfig" {
  value = scaleway_k8s_cluster.mon_cluster.kubeconfig[0].config_file
  sensitive = true
}
