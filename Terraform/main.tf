resource "scaleway_k8s_cluster" "mon_cluster" {
  name    = "cluster-k8s-aiproject"
  version = "1.29.1"
  cni     = "cilium"
  tags    = ["terraform", "k8s"]
  private_network_id = scaleway_vpc_private_network.my_private_network.id

  autoscaler_config {
    disable_scale_down = false
    estimator = "binpacking"
    expander = "random"
    ignore_daemonsets_utilization = true
    balance_similar_node_groups = true
    expendable_pods_priority_cutoff = -10
  }

  delete_additional_resources = true
}

resource "scaleway_vpc_private_network" "my_private_network" {
  name   = "my-private-network"
  region = "fr-par"
}
